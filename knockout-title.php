<?php
/*
Plugin Name: Knockout Title
Plugin URI: https://bitbucket.org/austinjp/knockout-title
Description: Remove titles from most pages and posts. Note: titles are still shown on some pages (search results, 404, category list, tag list, archive pages, author pages, attachment pages.)
Version: 0.0.2
Author: Austin Plunkett
Author URI: https://bitbucket.org/austinjp
*/

/*
  Hat tip to Smashing Magazine for their tutorial at
  https://www.smashingmagazine.com/2011/10/create-custom-post-meta-boxes-wordpress/
*/

function blank_title($title) {
    if (in_the_loop() && title_should_be_hidden(get_the_ID(), $title)) {
        return false;
    }
    return $title;
}
add_filter('the_title', 'blank_title');

function title_should_be_hidden($post_id, $title) {
    global $wp_query;
    if(get_post_meta($post_id,'knockout_title_class', true) &&
       ! ($wp_query->is_404 ||
          $wp_query->is_archive ||
          $wp_query->is_attachment ||
          $wp_query->is_author ||
          $wp_query->is_category ||
          $wp_query->is_month ||
          $wp_query->is_search ||
          $wp_query->is_tag ||
          $wp_query->is_year
         )) {
        return true;
    }
    return false;
}

add_action('load-post.php','knockout_title_metabox_setup');
add_action('load-post-new.php','knockout_title_metabox_setup');

function knockout_title_metabox_setup() {
    add_action('add_meta_boxes','knockout_title_metabox');
    add_action('save_post','knockout_title_save');
}

function knockout_title_metabox() {
    add_meta_box(
        'knockout_title_class',        // ID
        'Knockout Title',              // title to display
        'knockout_title_metabox_html', // callback func
        [ 'post', 'page' ],            // post types
        'side',                        // context
        'default'                      // priority
    );
}

function knockout_title_metabox_html() {
    global $post;
    wp_nonce_field( basename( __FILE__ ), 'knockout_title_class_nonce' ); ?>
    <p>
      <label><input name="knockout_title_class" id="knockout_title_class" type="checkbox" value="1" <?php if(get_post_meta($post->ID,'knockout_title_class', true) != 0) { echo 'checked'; } ?>>Hide the title</label>
    </p>
    <p>
      Update or save the post to set.
    </p>
<?php }

function knockout_title_save($post_id, $post = null) {
    if (null !== $post) { return; }
    if ( !isset( $_POST['knockout_title_class_nonce'] ) || !wp_verify_nonce( $_POST['knockout_title_class_nonce'], basename( __FILE__ ) ) )
        return $post_id;

    $post_type = get_post_type_object( $post->post_type );

    /* Check if the current user has permission to edit the post. */
    if ( !current_user_can( $post_type->cap->edit_post, $post_id ) )
        return $post_id;

    $new_meta_value = isset( $_POST['knockout_title_class'] ) ? true : false;

    $meta_key = 'knockout_title_class';

    if ($new_meta_value) {
        add_post_meta( $post_id, $meta_key, 1, true );
    } else {
        delete_post_meta($post_id, $meta_key);
    }
}
