# Knockout Title

Very simple plugin to remove titles from Wordpress posts and pages.

A metabox appears on the edit screen for posts and pages. Check the box, save the page, and the title will be removed.

This plugin overrised Wordpress' `the_title()` function and completely removes the title HTML. It does not use CSS or JavaScript.
